import os, epdtdi_timber, datetime, pytz, re
import pytimber
from collections import OrderedDict
from django.conf import settings
from django.http import HttpResponseRedirect
# import matplotlib.pyplot as pl
# from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
# from matplotlib.figure import Figure

# pl.switch_backend('agg')

# colors = ['#3366CC','#DC3912','#FF9900','#109618','#990099','#3B3EAC','#0099C6','#DD4477',
# '#66AA00','#B82E2E','#316395','#994499','#22AA99','#AAAA11','#6633CC','#E67300','#8B0707',
# '#329262','#5574A6','#3B3EAC']

def userAuthorised(request, app_vars):
    # if user attempts to view a non-configured view, gracefully send to the default
    if app_vars not in settings.APPS_CONFIGURED.keys():
        return HttpResponseRedirect('/app?app_error=' + app_vars)
    # if production env, SSO will have defined the user's egroups as HTTP header
    # else dev env so don't expect HTTP security headers
    if "HTTP_X_FORWARDED_GROUP" in request.META.keys():
        # collect list of egroups from HTTP header
        authenticatedGroups = request.META["HTTP_X_FORWARDED_GROUP"]
        requiredGroup = settings.APPS_CONFIGURED[app_vars]
        if settings.ADMIN_EGROUP not in authenticatedGroups and settings.APPS_CONFIGURED[app_vars] not in authenticatedGroups:
            print("Yikes! Not authorised to access that application!")
            return HttpResponseRedirect('/app?app_error=' + app_vars + '&group_error=' + requiredGroup)
        else:
            print("User adequately authorised with the '" + requiredGroup + "' egroup.")
    return True

def getToday():
    today = datetime.datetime.today()
    todayStr = today.strftime('%m/%d/%Y %I:%M %p')
    fromTS = todayStr
    return fromTS, fromTS

def getUTC(fromDTP, toDTP, tzone):
    fromDT = datetime.datetime.strptime(fromDTP, '%m/%d/%Y %I:%M %p')
    toDT = datetime.datetime.strptime(toDTP, '%m/%d/%Y %I:%M %p')
    # To get correct data it's best to work with UTC
    # Set the time zone
    fromDT = pytz.timezone(tzone).normalize(pytz.timezone(tzone).localize(fromDT))
    print(fromDT.strftime('%m/%d/%Y %I:%M %p'), fromDT.tzinfo) # Return time zone info
    # Transform the time to UTC
    fromDT = fromDT.astimezone(pytz.utc)
    print(fromDT.strftime('%m/%d/%Y %I:%M %p'), fromDT.tzinfo)

    # Set the time zone
    toDT = pytz.timezone(tzone).normalize(pytz.timezone(tzone).localize(toDT))
    print(toDT.strftime('%m/%d/%Y %I:%M %p'), toDT.tzinfo) # Return time zone info
    # Transform the time to UTC
    toDT = toDT.astimezone(pytz.utc)
    print(toDT.strftime('%m/%d/%Y %I:%M %p'), toDT.tzinfo)

    fromUTCsecs = (fromDT-datetime.datetime(1970,1,1, tzinfo=pytz.utc)).total_seconds()
    toUTCsecs = (toDT-datetime.datetime(1970,1,1, tzinfo=pytz.utc)).total_seconds()
    return fromUTCsecs, toUTCsecs

def getVarsAndUnits(app_vars):
    gifppVariables = []
    gifppVarsAndUnits = {}
    gifppVarsAndTypes = {}
    file = "static/" + app_vars + "_vars_grouped.csv" if len(app_vars) > 0 else "static/vars_grouped.csv"
    varFile = os.path.join(os.path.dirname(epdtdi_timber.__file__), file)
    with open(varFile) as f:
        # read file from third line down
        content = f.readlines()[2:]
    # you may also want to remove whitespace characters like `\n` at the end of each line
    content = [x.strip() for x in content]
    for e in content:
        varAndUnit = e.split(',')
        if len(varAndUnit) > 1:
            gifppVariables.append(varAndUnit[0])
            gifppVarsAndUnits[varAndUnit[0]] = varAndUnit[1]
            gifppVarsAndTypes[varAndUnit[0]] = varAndUnit[2]

    return gifppVariables, gifppVarsAndUnits, gifppVarsAndTypes

def getGroupedVars(app_vars):
    file = "static/" + app_vars + "_vars_grouped.csv" if len(app_vars) > 0 else "static/vars_grouped.csv"
    groupedVars = OrderedDict()
    gvarFile = os.path.join(os.path.dirname(epdtdi_timber.__file__), file)
    with open(gvarFile) as g:
        # read file from third line down
        content = g.readlines()[2:]
    # you may also want to remove whitespace characters like `\n` at the end of each line
    content = [x.strip() for x in content]
    for e in content:       
        if e.startswith("# "): # fix entries without metric
            group = e.strip('# ').strip(',')
            groupedVars[group] = []
        else:
            varAndUnit = e.split(',')
            tup=(varAndUnit[0], varAndUnit[1])
            groupedVars[str(group)].append(tup)
    return groupedVars


# Used in old PyTimber app for static plot

# def makeTrPlot(fromUTC, toUTC, what, app_vars):
#     print("Want to plot from: ")
#     print(fromUTC)
#     print(toUTC)

#     # if no vars requested send back picture of logs - should never successfully be requested
#     if what == "None":
#         return 'timber-logs.jpg', None, None

#     gifppVariables, gifppVarsAndUnits, gifppVarsAndTypes = getVarsAndUnits(app_vars)

#     for var_idx, var in enumerate(what):
#         if gifppVarsAndTypes[var] == "string":
#             del what[var_idx]

#     # timestamp start of database transaction
#     startedTime = datetime.datetime.now()
#     # setup and connect to DB
#     ldb = pytimber.LoggingDB()
#     # send request and await response
#     data=ldb.get(what, float(fromUTC), float(toUTC))
#     # timestamp response from DB
#     finishedTime = datetime.datetime.now()
#     # calculate elapsed time of DB connection and request
#     elapsed = (finishedTime - startedTime).total_seconds()

#     print("[{:%d/%b/%Y %H:%M:%S}] ".format(finishedTime), "Got data! Took ", elapsed * 1000, "ms (", elapsed, "s) to retrieve data.", sep='')

#     fig = Figure(figsize=(15,10))  # Plotting
#     canvas = FigureCanvas(fig)
#     ax = fig.add_subplot(111)

#     color = 0
#     for var in what:
#         tt, vv = data[var]
#         if gifppVarsAndTypes[var] == "boolean":
#             ax.step(tt, vv, colors[color % len(colors)], label=var+' ['+gifppVarsAndUnits[var]+']', marker='o')
#         else:
#             ax.plot(tt, vv, colors[color % len(colors)], label=var+' ['+gifppVarsAndUnits[var]+']', marker='x')
#         color += 1

#     title = "Variables: " + ", ".join(what)

#     ax.legend()
#     ax.set_title(title)
#     ax.grid(True)
#     ax.set_xlabel('x')
#     ax.set_ylabel('y')

#     pytimber.set_xaxis_date(ax)

#     image_name = app_vars + '_timber.png'

#     canvas.print_figure(os.path.join(os.path.dirname(epdtdi_timber.__file__), 'static/' + image_name))
#     return image_name, data, elapsed, gifppVarsAndUnits, gifppVarsAndTypes

def getData(fromUTC, toUTC, what, app_vars):
    if what == "None":
        return None
    gifppVariables, gifppVarsAndUnits, gifppVarsAndTypes = getVarsAndUnits(app_vars)
    # timestamp start of database transaction
    startedTime = datetime.datetime.now()
    print("[{:%d/%b/%Y %H:%M:%S}]".format(startedTime), "Want data from: ")
    print(", ".join(what))
    print(fromUTC)
    print(toUTC)
    # setup and connect to DB
    ldb = pytimber.LoggingDB()
    # send request and await response
    data=ldb.get(what, float(fromUTC), float(toUTC))
    # timestamp response from DB
    finishedTime = datetime.datetime.now()
    # calculate elapsed time of DB connection and request
    elapsed = (finishedTime - startedTime).total_seconds()
    print("[{:%d/%b/%Y %H:%M:%S}] ".format(finishedTime), "Got data! Took ", elapsed * 1000, "ms (", elapsed, "s) to retrieve data.", sep='')
    return data, elapsed, gifppVarsAndUnits, gifppVarsAndTypes

def getDipVals(app_vars):
    subs = settings.SUBSCRIPTIONS[app_vars]
    data = {}
    for sub_name, sub_obj in subs.items():
        data[sub_name] = {
            'value': sub_obj['dip_sub'].current_val,
            'unit': sub_obj['dip_unit'],
            'format': sub_obj['dip_format'],
            'flo_display': sub_obj['dip_float_display'],
            'sig_fig': sub_obj['dip_sig_fig'],
            'lower_limit': sub_obj['dip_lower_limit'],
            'upper_limit': sub_obj['dip_upper_limit'],
            'group': sub_obj['dip_group']
        }
    return data

def getStructDipVals(app_vars):
    subs_struct = settings.SUBSCRIPTIONS_STRUCT[app_vars]
    data = {}
    for key, var in subs_struct.items():
        temp = var['dip_sub'].current_val
        for sub_name, sub_obj in var['vars'].items():
            data[sub_name] = {
                'value': temp[sub_name],
                'unit': sub_obj['dip_unit'],
                'format': sub_obj['dip_format'],
                'flo_display': sub_obj['dip_float_display'],
                'sig_fig': sub_obj['dip_sig_fig'],
                'lower_limit': sub_obj['dip_lower_limit'],
                'upper_limit': sub_obj['dip_upper_limit'],
                'group': sub_obj['dip_group']
        }
    return data